namespace CovidPass.Models;

public class Certificate {
  public int Id { get; set; }
  public string? FirstName { get; set; }
  public string? LastName { get; set; }
  public DateTime? InsertedAt { get; set; }
  public int SerialNumber { get; set; }
  public string? Label => string.Format("{0}{1}{2}", FirstName?[0], LastName?[0], SerialNumber);

  public string? IdentityUserId { get; set; }
  public IdentityUser? IdentityUser { get; set; }
}